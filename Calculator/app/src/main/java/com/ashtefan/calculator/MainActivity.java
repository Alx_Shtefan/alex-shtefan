package com.ashtefan.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button one, two, three, four, five, six, seven, eight, nine, zero;
    Button plus, minus, multiply, difine, dot, delete, equally;
    TextView board;
    String num;
    View.OnClickListener number, operation;

    int num_of_operation;

    float num_first, num_second, answer;

    private void setId() {
        one = (Button) findViewById(R.id.button_1);
        two = (Button) findViewById(R.id.button_2);
        three = (Button) findViewById(R.id.button_3);
        four = (Button) findViewById(R.id.button_4);
        five = (Button) findViewById(R.id.button_5);
        six = (Button) findViewById(R.id.button_6);
        seven = (Button) findViewById(R.id.button_7);
        eight = (Button) findViewById(R.id.button_8);
        nine = (Button) findViewById(R.id.button_9);
        zero = (Button) findViewById(R.id.button_0);

        plus = (Button) findViewById(R.id.plus);
        minus = (Button) findViewById(R.id.minus);
        multiply = (Button) findViewById(R.id.multiply);
        difine = (Button) findViewById(R.id.difine);
        dot = (Button) findViewById(R.id.dot);
        delete = (Button) findViewById(R.id.delete);
        equally = (Button) findViewById(R.id.equally);

        board = (TextView) findViewById(R.id.nums);
    }

    private void inputNumbers() {
        number = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.button_1: {
                        num = board.getText().toString();
                        if (num.equals(""))
                        {
                            num = "1";
                        } else {
                            num = num + "1";
                        }
                        board.setText(num);
                        break;
                    }
                    case R.id.button_2: {
                        num = board.getText().toString();
                        if (num.equals(""))
                        {
                            num = "2";
                        } else {
                            num = num + "2";
                        }
                        board.setText(num);
                        break;
                    }
                    case R.id.button_3: {
                        num = board.getText().toString();
                        if (num.equals("")) {
                            num = "3";
                        } else {
                            num = num + "3";
                        }
                        board.setText(num);
                        break;
                    }
                    case R.id.button_4: {
                        num = board.getText().toString();
                        if (num.equals("")) {
                            num = "4";
                        } else {
                            num = num + "4";
                        }
                        board.setText(num);
                        break;
                    }
                    case R.id.button_5: {
                        num = board.getText().toString();
                        if (num.equals("")) {
                            num = "5";
                        } else {
                            num = num + "5";
                        }
                        board.setText(num);
                        break;
                    }
                    case R.id.button_6: {
                        num = board.getText().toString();
                        if (num.equals("")) {
                            num = "6";
                        } else {
                            num = num + "6";
                        }
                        board.setText(num);
                        break;
                    }
                    case R.id.button_7: {
                        num = board.getText().toString();
                        if (num.equals("")) {
                            num = "7";
                        } else {
                            num = num + "7";
                        }
                        board.setText(num);
                        break;
                    }
                    case R.id.button_8: {
                        num = board.getText().toString();
                        if (num.equals("")) {
                            num = "8";
                        } else {
                            num = num + "8";
                        }
                        board.setText(num);
                        break;
                    }
                    case R.id.button_9: {
                        num = board.getText().toString();
                        if (num.equals("")) {
                            num = "9";
                        } else {
                            num = num + "9";
                        }
                        board.setText(num);
                        break;
                    }
                    case R.id.button_0: {
                        num = board.getText().toString();
                        if (num.equals("")) {
                            num = "0";
                        } else {
                            num = num + "0";
                        }
                        board.setText(num);
                        break;
                    }
                }
            }
        };

        one.setOnClickListener(number);
        two.setOnClickListener(number);
        three.setOnClickListener(number);
        four.setOnClickListener(number);
        five.setOnClickListener(number);
        six.setOnClickListener(number);
        seven.setOnClickListener(number);
        eight.setOnClickListener(number);
        nine.setOnClickListener(number);
        zero.setOnClickListener(number);
    }

    private void makeOperations() {
        operation = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.plus: {
                        num = board.getText().toString();
                        if (num == ""){
                            break;
                        }
                        num_first = Float.parseFloat(num);
                        board.setText("");
                        board.setHint("0");
                        num_of_operation = 1;
                        break;
                    }
                    case R.id.minus: {
                        num = board.getText().toString();
                        if (num == ""){
                            break;
                        }
                        num_first = Float.parseFloat(num);
                        board.setText("");
                        board.setHint("0");
                        num_of_operation = 2;
                        break;
                    }
                    case R.id.multiply: {
                        num = board.getText().toString();
                        if (num == ""){
                            break;
                        }
                        num_first = Float.parseFloat(num);
                        board.setText("");
                        board.setHint("0");
                        num_of_operation = 3;
                        break;
                    }
                    case R.id.difine: {
                        num = board.getText().toString();
                        if (num == ""){
                            break;
                        }
                        num_first = Float.parseFloat(num);
                        board.setText("");
                        board.setHint("0");
                        num_of_operation = 4;
                        break;
                    }
                    case R.id.dot: {
                        if (num == ""){
                            break;
                        }
                        num = board.getText().toString();
                        if (num.charAt(num.length()-1) == '.') {
                            break;
                        }
                        num = num + ".";
                        board.setText(num);
                        break;
                    }
                    case R.id.delete: {
                        num = board.getText().toString();
                        if (num.length() != 0) {
                            num = num.substring(0, num.length()-1);
                            board.setText(num);
                            break;
                        } else {
                            break;
                        }
                    }

                    case R.id.equally: {

                        num = board.getText().toString();

                        if (num.length() != 0) {
                            num_second = Float.parseFloat(num);
                            switch (num_of_operation) {
                                case 1: {
                                    answer = num_first + num_second;
                                    num = Float.toString(answer);
                                    board.setText(num);
                                    break;
                                }
                                case 2: {
                                    answer = num_first - num_second;
                                    num = Float.toString(answer);
                                    board.setText(num);
                                    break;
                                }
                                case 3: {
                                    answer = num_first * num_second;
                                    num = Float.toString(answer);
                                    board.setText(num);
                                    break;
                                }
                                case 4: {
                                    if (num_second != 0) {
                                        answer = 0f;
                                        answer = num_first / num_second;

                                        num = Float.toString(answer);
                                        board.setText(num);
                                        break;
                                    } else {
                                        board.setText("Error");
                                        break;
                                    }
                                }
                            }
                        } else {
                            break;
                        }
                        num_first = 0;
                        num_second = 0;
                    }
                }
            }
        };

        plus.setOnClickListener(operation);
        minus.setOnClickListener(operation);
        multiply.setOnClickListener(operation);
        difine.setOnClickListener(operation);
        dot.setOnClickListener(operation);
        delete.setOnClickListener(operation);
        equally.setOnClickListener(operation);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setId();

        num = "";
        num_first = 0f;
        num_second = 0f;

        num_of_operation = 0;

        inputNumbers();

        makeOperations();

        View.OnLongClickListener fully_delete = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                board.setText("");
                board.setHint("0");
                return true;
            }
        };
        delete.setOnLongClickListener(fully_delete);
    }
}

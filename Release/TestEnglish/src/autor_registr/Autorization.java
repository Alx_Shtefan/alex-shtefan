package autor_registr;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;

import choose_of_tests.Main;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;

import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class Autorization {

	private JFrame frame = null;
	private JLabel lblProblem = null;
	private JTextField dtrpnUserName = null;
	private JPasswordField passwordField = null;
	private int up_down = 1;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Autorization window = new Autorization();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Autorization() {
		initialize();
	}

	private void initialize() {
		KeyHandler listener = new KeyHandler();
		
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 483, 470);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Authorization");
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(240, 230, 140));
		panel.setBounds(0, 0, 479, 444);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
	    lblProblem = new JLabel("");
		// �������� �����
		lblProblem.setFont(new Font("Georgia", Font.ITALIC, 20));
		lblProblem.setForeground(Color.RED);
		lblProblem.setBounds(16, 368, 453, 32);
		panel.add(lblProblem);
		
		dtrpnUserName = new JTextField(); 
		// ���� ����� ������������
		dtrpnUserName.setToolTipText("");
		dtrpnUserName.setFont(new Font("Georgia", Font.PLAIN, 36));
		dtrpnUserName.setBounds(123, 137, 277, 51);
		panel.add(dtrpnUserName);
		dtrpnUserName.addKeyListener(listener);
		
		passwordField = new JPasswordField();
		//���� ������
		passwordField.setFont(new Font("Tahoma", Font.PLAIN, 36));
		passwordField.setBounds(123, 208, 277, 51);
		panel.add(passwordField);
		passwordField.addKeyListener(listener);
		
		JLabel lblNewLabel = new JLabel("         \u0410uthentication");
		lblNewLabel.setFont(new Font("Georgia", Font.ITALIC, 40));
		lblNewLabel.setBounds(24, 16, 455, 65);
		panel.add(lblNewLabel);
		
		///////////////////////////////////////////////////////////////
		
		JButton btnLogIn = new JButton("Sign up");
		btnLogIn.setBackground(new Color(255, 250, 205));
		// ������ ������������
		btnLogIn.setFont(new Font("Georgia", Font.PLAIN, 26));
		btnLogIn.setBounds(125, 282, 130, 58);
		panel.add(btnLogIn);
		
		btnLogIn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				changeFrameToRegistr();
			}
		});
		
		JButton btnSignUp = new JButton("OK");
		btnSignUp.setBackground(new Color(255, 250, 205));
		// ������ ����� � �������
		btnSignUp.setFont(new Font("Georgia", Font.PLAIN, 26));
		btnSignUp.setBounds(270, 282, 130, 58);
		panel.add(btnSignUp);
		btnSignUp.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				checkOnCorrect();
			}
		});
		
		JLabel lblPassword = new JLabel("password");
		lblPassword.setFont(new Font("Georgia", Font.PLAIN, 20));
		lblPassword.setBounds(16, 228, 97, 25);
		panel.add(lblPassword);
		
		JLabel lblNewLabel_1 = new JLabel("username");
		lblNewLabel_1.setBackground(new Color(240, 240, 240));
		lblNewLabel_1.setFont(new Font("Georgia", Font.PLAIN, 20));
		lblNewLabel_1.setBounds(17, 153, 97, 20);
		panel.add(lblNewLabel_1);
		
	}
	
	public void checkOnCorrect() {
		boolean flag = true;
		String s = dtrpnUserName.getText().toLowerCase();
		char [] a = passwordField.getPassword();
		char [] b = s.toCharArray();
		
		if (a.length > b.length) {
			for (int i = 0; i < b.length; i++) {
				if (a[i] == ' ' || b[i] == ' ') {
					flag = false;
				}
			}
		} else {
			for (int i = 0; i < a.length; i++) {
				if (a[i] == ' ' || b[i] == ' ') {
					flag = false;
				}
			}
		}

		if (String.valueOf(a).isEmpty() && dtrpnUserName.getText().isEmpty()) {
			lblProblem.setText("Please, enter your username and password");
		} else if (a.length == 0) {
			lblProblem.setText("Please, enter your password");
		} else if (!flag) {
			lblProblem.setText("Please, don't use SPACE");
		} else {
			lblProblem.setText("");
			WorkWithAuthorithation wwa = new WorkWithAuthorithation(dtrpnUserName.getText(), String.valueOf(a)); //����� ������ �� ����������
			try {
				result(wwa.getWorkResult());
			} catch (ClassNotFoundException | IOException e1) {
				e1.printStackTrace();
			}// �������� ��������� ���������
		}		
	}
	
	public void changeFrameToRegistr() {
		Registration r = new Registration();
		r.change();
		frame.setVisible(false);
	}
	
	public void change() {
		frame.setVisible(true);
	}
	
	public void result(int result) throws ClassNotFoundException, IOException 
	/* ��������� ������������� ���������� */ {
		if (result == 0) {
			lblProblem.setText("Your password is incorrect!");
		} else if (result == 1) {
			lblProblem.setText("You are already in!");
			frame.dispose();
			Main m = new Main();
			m.setUsername(dtrpnUserName.getText());
			m.change();
			m.setDict();
		} else if (result == -1) {
			changeFrameToRegistr();
		}
	}
	
	private class KeyHandler implements KeyListener {

		@Override
		public void keyPressed(KeyEvent event) {
			int keyCode = event.getKeyCode();
			switch (keyCode) {
				case KeyEvent.VK_UP:
					if (up_down == 1) {
						up_down = 2;
						passwordField.requestFocus();
					} else if (up_down == 2) {
						up_down = 1;
						dtrpnUserName.requestFocus();
					}
					break;
				case KeyEvent.VK_DOWN:
					if (up_down == 2) {
						up_down = 1;
						dtrpnUserName.requestFocus();
					} else if (up_down == 1) {
						up_down = 2;
						passwordField.requestFocus();
					}
					break;	
				case KeyEvent.VK_ENTER:
					checkOnCorrect();
					break;
			}
		}

		@Override
		public void keyReleased(KeyEvent event) {
			// TODO Auto-generated method stub
		}

		@Override
		public void keyTyped(KeyEvent event) {
			// TODO Auto-generated method stub
		}
	}
}
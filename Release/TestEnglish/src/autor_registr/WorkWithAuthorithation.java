package autor_registr;

public class WorkWithAuthorithation {
	
	private Authorization a = new Authorization();
	private int result = 100;
	
	public WorkWithAuthorithation(String username, String password) {
		if (a.isIn(username)) { // ���� �������� ��� ������������ ���� � ����
			setWorkResult(a.isCorrect(username, password));
		} else { // ���� ��� ��������� �� �����������
			setWorkResult(-1);
		}
	}
	
	public void setWorkResult(int result) { // 1 ���� ��� �������, 0 ���� �� ���������� ������, -1 ���� ����� ��� � ����
		this.result = result;
	}
	
	public int getWorkResult() {
		return result;
	}
}
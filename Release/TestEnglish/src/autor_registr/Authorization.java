package autor_registr;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class Authorization implements Serializable {

	private static final long serialVersionUID = 1L;
	private static ArrayList<HashMap<String, String>> users = new ArrayList<>();

	public Authorization() 
	/* ��� �������� ������ ������ ��������� � ��������� ��� ���� */{
		try {
			getFromFile();
		} catch (ClassNotFoundException | IOException e) {}	
	}
	
	@SuppressWarnings("unchecked")
	public void getFromFile() throws IOException, ClassNotFoundException
	/* ��������� �� ����� ���������� � ������ � �� ������� � HashMap */{		 
		File f = new File("Files/users.txt");
		ObjectInputStream ois = null;
		ois = new ObjectInputStream(new FileInputStream(f));
		users = (ArrayList<HashMap<String, String>>) ois.readObject();
		ois.close();
	}
	
	public static void sendToFile(ArrayList<HashMap<String, String>> users2) throws IOException 
	/* ���������� � ���� ���������� � ������ � �� ������� �� HashMap */{		
		File f = new File("Files/users.txt");		
		ObjectOutputStream oos = null;		
		FileOutputStream fos = new FileOutputStream(f);
		if (fos != null) {
			oos = new ObjectOutputStream(fos);
			oos.writeObject(users2);
		}		
		oos.close();
	}
	
	public boolean isIn(String username) 
	/* ��������� ������� ���������� ������ username*/{
		for (int i = 0; i < users.size(); i++) {
			if (users.get(i).containsKey(username)) {
				return true;
			}
		}
		return false;
	}
	
	public int isCorrect(String username, String password) {
	/* ��������� ���������� �� password � username �������� ������������� */
		for (int i = 0; i < users.size(); i++) {
			HashMap<String, String> m = users.get(i);
			if (m.containsKey(username)) { 
				if (m.containsValue(password)) {
					return 1; // ���� ��� ������� 
				} else {
					return 0; // ���� �� ���������� ������
				}
			} 
		}
		return -1;
	}

	public void addNewUser(String username, String password) throws IOException, ClassNotFoundException {
		HashMap<String, String> m = new HashMap<String, String>();
		m.put(username, password);
		users.add(m);
		sendToFile(users);
		getFromFile();
	}
}
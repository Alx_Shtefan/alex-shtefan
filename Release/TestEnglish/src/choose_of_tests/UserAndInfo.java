package choose_of_tests;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class UserAndInfo implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private ArrayList<HashMap<String, ArrayList<String>>> learningWords = new ArrayList<>(); // ��������� ����� File -> learningWords
	private ArrayList<HashMap<String, ArrayList<String>>> learnedWords = new ArrayList<>(); // �������� ����� File -> learnedWords
	
	// �������� �������� � �������� ��� ���������� ���������� �������
	
	public ArrayList<HashMap<String, ArrayList<String>>> getLearningWords() {
		return learningWords;
	}
	
	public void setLearningWords(ArrayList<HashMap<String, ArrayList<String>>> learningWords) {
		this.learningWords = learningWords;
	}
	
	public ArrayList<HashMap<String, ArrayList<String>>> getLearnedWords() {
		return learnedWords;
	}
	
	public void setLearnedWords(ArrayList<HashMap<String, ArrayList<String>>> learnedWords) {
		this.learnedWords = learnedWords;
	}
} 
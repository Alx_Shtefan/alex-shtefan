package choose_of_tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;


import dictionary.Dictionary;

public class UsersInfoAboutDictionaries {
// ����� ���� record ���������� �� ��� ������� ������������
	
	private Dictionary d = new Dictionary(Main.getSettings());
	private ArrayList<Integer> numbers = new ArrayList<Integer>();// ��� �� �� �����������
	private ArrayList<HashMap<String, UserAndInfo>> users = new ArrayList<HashMap<String, UserAndInfo>>(); // ������ ������������� � �� ��������
	private UserAndInfo uai = new UserAndInfo(); // ���� ����������� �� �������
	
	private String username = ""; // ��� ������������
	private HashMap<String, UserAndInfo> info = new HashMap<String, UserAndInfo>(); // ���������� ��� ������� � ���������
	
	
	public UsersInfoAboutDictionaries(String username) throws ClassNotFoundException, IOException {
		
		this.username = username;

		getFromFile();
		
		if (!checkOnFirstTime()) {
			fillFromTheBegining();
			info.clear();
			info.put(username, uai);
			users.add(info);
			sendToFile(users);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void getFromFile() throws IOException, ClassNotFoundException
	/* ��������� �� ����� ���������� � ������ � �� ������� � HashMap */{
		 
		File f = new File("Files/Dictionaries/usersDict.txt");
		
		ObjectInputStream ois = null;
		
		FileInputStream fis = new FileInputStream(f);
		ois = new ObjectInputStream(fis);
		users = (ArrayList<HashMap<String, UserAndInfo>>) ois.readObject();
			
		ois.close();
	}
	
	public static void sendToFile(ArrayList<HashMap<String, UserAndInfo>> a) throws IOException 
	/* ���������� � ���� ���������� � ������ � �� ������� �� HashMap */{
		
		File f = new File("Files/Dictionaries/usersDict.txt");
		
		ObjectOutputStream oos = null;
		
		FileOutputStream fos = new FileOutputStream(f);
		if (fos != null) {
			oos = new ObjectOutputStream(fos);
			oos.writeObject(a);
		}
		
		oos.close();
	}
	
	public void fillFromTheBegining() {	
	// ���� ������� ������ ���, �� �������� ��� ������� ��������� 15 ������ �������	
		int i = 0;
		while (i != Main.getSettingAmountOfWords()) {
			int count = (int) new Random().nextInt(d.getLength());
			if (numbers.contains(count)) {
				continue;
			} else {
				i++;
				numbers.add(count);
				uai.getLearningWords().add(d.getWordFromMainDictionary(count));
			}
		}
	}
	
	public boolean checkOnFirstTime() {
	// �������� �� ������ ���	
		boolean b = false;
		for (int i = 0; i < users.size(); i++) {
			if (users.get(i).containsKey(username)) {
				b = true;
				info = users.get(i);
			}
		}
		return b;
	}
	
	public ArrayList<HashMap<String, ArrayList<String>>> getLerning() {
		return info.get(username).getLearningWords();
	}
	
	public ArrayList<HashMap<String, ArrayList<String>>> getLearned() {
		return info.get(username).getLearnedWords();
	}
	
	public void updateDict(String username) {
		
		checkOnFirstTime();
		uai = new UserAndInfo();
		uai.setLearnedWords(Main.getLerned());
		uai.setLearningWords(Main.getLerning());
		
		info.clear();
		info.put(username, uai);
		
		for (int i = 0; i < users.size(); i++) {
			if (users.get(i).containsKey(username)) {
				users.remove(i);
			}
		}
		
		users.add(info);
		
		try {
			sendToFile(users);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

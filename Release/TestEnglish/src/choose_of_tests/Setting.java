package choose_of_tests;

import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JRadioButton;
import javax.swing.JTextField;
import java.awt.Color;

public class Setting {

	private JFrame frmSetting;
	private String username = "";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Setting window = new Setting();
					window.frmSetting.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Setting() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmSetting = new JFrame();
		frmSetting.setResizable(false);
		frmSetting.setTitle("Setting");
		frmSetting.setBounds(100, 100, 584, 339);
		frmSetting.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSetting.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(240, 230, 140));
		panel.setBounds(0, 0, 578, 310);
		frmSetting.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblHowManyTr = new JLabel("How many variants of English translation of the word");
		lblHowManyTr.setForeground(new Color(75, 0, 130));
		lblHowManyTr.setFont(new Font("Georgia", Font.PLAIN, 20));
		lblHowManyTr.setBounds(10, 11, 553, 37);
		panel.add(lblHowManyTr);
		
		JLabel lblOfTheWords = new JLabel("do you want to upload in your dictionary?");
		lblOfTheWords.setForeground(new Color(75, 0, 130));
		lblOfTheWords.setFont(new Font("Georgia", Font.PLAIN, 20));
		lblOfTheWords.setBounds(10, 43, 553, 37);
		panel.add(lblOfTheWords);
		
		JRadioButton rbt1 = new JRadioButton("1 (one translation is enough)");
		rbt1.setForeground(new Color(153, 50, 204));
		rbt1.setBackground(new Color(240, 230, 140));
		rbt1.setFont(new Font("Tahoma", Font.PLAIN, 25));
		rbt1.setBounds(31, 87, 393, 49);
		panel.add(rbt1);
		
		JRadioButton rbt2 = new JRadioButton("2 (only two translations)");
		rbt2.setForeground(new Color(153, 50, 204));
		rbt2.setBackground(new Color(240, 230, 140));
		rbt2.setFont(new Font("Tahoma", Font.PLAIN, 25));
		rbt2.setBounds(31, 139, 393, 49);
		rbt2.setSelected(true);
		panel.add(rbt2);
		
		ButtonGroup bg = new ButtonGroup();
		bg.add(rbt1);
		bg.add(rbt2);
		
		JButton btn = new JButton("Save");
		btn.setForeground(new Color(148, 0, 211));
		btn.setBackground(new Color(255, 250, 205));
		btn.setFont(new Font("Georgia", Font.PLAIN, 20));
		btn.setBounds(474, 253, 89, 33);
		panel.add(btn);
		
		JLabel lblAmountOfWords = new JLabel("Amount of words in your dictionary must be:");
		lblAmountOfWords.setForeground(new Color(75, 0, 130));
		lblAmountOfWords.setFont(new Font("Georgia", Font.PLAIN, 20));
		lblAmountOfWords.setBounds(15, 204, 410, 33);
		panel.add(lblAmountOfWords);
		
		JTextField editorPane = new JTextField();
		editorPane.setForeground(new Color(0, 0, 0));
		editorPane.setText(String.valueOf(Main.getSettingAmountOfWords()));
		editorPane.setFont(new Font("Microsoft YaHei UI Light", Font.BOLD, 20));
		editorPane.setBounds(430, 204, 47, 26);
		panel.add(editorPane);
		
		JLabel lblnoLessThen = new JLabel("(no less then 15)");
		lblnoLessThen.setForeground(new Color(75, 0, 130));
		lblnoLessThen.setFont(new Font("Georgia", Font.PLAIN, 15));
		lblnoLessThen.setBounds(137, 223, 119, 33);
		panel.add(lblnoLessThen);
		btn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Main m = new Main();
				m.change();
				m.setUsername(username);
				if (rbt1.isSelected()) {
					m.setSettingType(1);
				} else {
					m.setSettingType(2);
				}
				try {
					int p = Integer.parseInt(editorPane.getText());
					if (p >= 15) {
						m.setSettingAmountOfWords(p);
					} else {
						m.setSettingAmountOfWords(15);
					}
					
				} catch (NumberFormatException e) {
					System.err.println("default value, because of not digit in the field");
				}
				frmSetting.dispose();
			}
		});
	}
	
	public void change(String username) {
		this.username = username;
		frmSetting.setVisible(true);
	}
}

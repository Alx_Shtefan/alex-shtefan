package dictionary;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Color;

public class Error {

	private JFrame frmError;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Error window = new Error();
					window.frmError.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Error() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmError = new JFrame();
		frmError.setTitle("Error");
		frmError.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent arg0) {
				ChooseOfTheDictionary.setClick(0);
			}
		});
		frmError.setResizable(false);
		frmError.setBounds(100, 100, 347, 141);
		frmError.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmError.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(240, 230, 140));
		panel.setBounds(0, 0, 341, 112);
		frmError.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblYouHaveNo = new JLabel("You have no words");
		lblYouHaveNo.setForeground(new Color(255, 0, 0));
		lblYouHaveNo.setBounds(10, 5, 321, 44);
		lblYouHaveNo.setFont(new Font("Georgia", Font.PLAIN, 36));
		panel.add(lblYouHaveNo);
		
		JLabel lblInThisDictionary = new JLabel("in this dictionary!");
		lblInThisDictionary.setForeground(Color.RED);
		lblInThisDictionary.setFont(new Font("Georgia", Font.PLAIN, 36));
		lblInThisDictionary.setBounds(20, 60, 293, 44);
		panel.add(lblInThisDictionary);
	}
	
	public void change() {
		frmError.setVisible(true);
	}
}

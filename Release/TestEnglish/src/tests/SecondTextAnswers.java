package tests;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.awt.Color;

import choose_of_tests.Main;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class SecondTextAnswers {

	protected JFrame frmAnswer;
	private int i = 0;
	private int j = 0;
	private String username = "";
	JLabel image_1;
	JLabel image_2;
	JLabel image_3;
	JLabel image_4;
	JLabel image_5;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SecondTextAnswers window = new SecondTextAnswers();
					window.frmAnswer.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SecondTextAnswers() {
		try {
			initialize();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws IOException 
	 */
	private void initialize() throws IOException {
		frmAnswer = new JFrame();
		frmAnswer.setResizable(false);
		frmAnswer.setTitle("Results of the Second Test");
		frmAnswer.setBounds(100, 100, 529, 300);
		frmAnswer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAnswer.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 228, 196));
		panel.setBounds(0, 0, 523, 271);
		frmAnswer.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Answers");
		lblNewLabel.setForeground(new Color(25, 25, 112));
		lblNewLabel.setFont(new Font("Ravie", Font.BOLD, 18));
		lblNewLabel.setBounds(141, 11, 123, 39);
		panel.add(lblNewLabel);
		
		JLabel eng_answer1 = new JLabel("");
		eng_answer1.setForeground(new Color(72, 61, 139));
		eng_answer1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		eng_answer1.setBounds(58, 48, 164, 26);
		panel.add(eng_answer1);
		eng_answer1.setText(SecondTest.englishAnswerWord.get(i));
		i++;
		
		JLabel eng_answer2 = new JLabel("");
		eng_answer2.setForeground(new Color(72, 61, 139));
		eng_answer2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		eng_answer2.setBounds(58, 85, 164, 26);
		panel.add(eng_answer2);
		eng_answer2.setText(SecondTest.englishAnswerWord.get(i));
		i++;
		
		JLabel eng_answer3 = new JLabel("");
		eng_answer3.setForeground(new Color(72, 61, 139));
		eng_answer3.setFont(new Font("Tahoma", Font.PLAIN, 20));
		eng_answer3.setBounds(58, 122, 164, 26);
		panel.add(eng_answer3);
		eng_answer3.setText(SecondTest.englishAnswerWord.get(i));
		i++;
		
		JLabel eng_answer4 = new JLabel("");
		eng_answer4.setForeground(new Color(72, 61, 139));
		eng_answer4.setFont(new Font("Tahoma", Font.PLAIN, 20));
		eng_answer4.setBounds(58, 159, 164, 26);
		panel.add(eng_answer4);
		eng_answer4.setText(SecondTest.englishAnswerWord.get(i));
		i++;
		
		JLabel eng_answer5 = new JLabel("");
		eng_answer5.setForeground(new Color(72, 61, 139));
		eng_answer5.setFont(new Font("Tahoma", Font.PLAIN, 20));
		eng_answer5.setBounds(58, 196, 164, 26);
		panel.add(eng_answer5);
		eng_answer5.setText(SecondTest.englishAnswerWord.get(i));
		
//////////////////////////////////////////////////////////////////////////////////////////////////
		TestsImplementation ti = new TestsImplementation(5);
		
		JLabel rus_answer1 = new JLabel(SecondTest.rusAnswerWords.get(j).get(0));
		if (ti.scan(SecondTest.rusAnswerWords.get(j)) != 1) {
			rus_answer1 = new JLabel(SecondTest.rusAnswerWords.get(j).get(0) + ", " + SecondTest.rusAnswerWords.get(j).get(1));
		}
		rus_answer1.setBounds(232, 48, 253, 26);
		panel.add(rus_answer1);
		rus_answer1.setForeground(new Color(72, 61, 139));
		rus_answer1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		j++;
		
		JLabel rus_answer2 = new JLabel(SecondTest.rusAnswerWords.get(j).get(0));
		if (ti.scan(SecondTest.rusAnswerWords.get(j)) != 1) {
			rus_answer2 = new JLabel(SecondTest.rusAnswerWords.get(j).get(0) + ", " + SecondTest.rusAnswerWords.get(j).get(1));
		}
		rus_answer2.setBounds(232, 85, 253, 26);
		panel.add(rus_answer2);
		rus_answer2.setForeground(new Color(72, 61, 139));
		rus_answer2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		j++;
		
		JLabel rus_answer3 = new JLabel(SecondTest.rusAnswerWords.get(j).get(0));
		if (ti.scan(SecondTest.rusAnswerWords.get(j)) != 1) {
			rus_answer3 = new JLabel(SecondTest.rusAnswerWords.get(j).get(0) + ", " + SecondTest.rusAnswerWords.get(j).get(1));
		}
		rus_answer3.setBounds(232, 122, 253, 26);
		panel.add(rus_answer3);
		rus_answer3.setForeground(new Color(72, 61, 139));
		rus_answer3.setFont(new Font("Tahoma", Font.PLAIN, 20));
		j++;
		
		JLabel rus_answer4 = new JLabel(SecondTest.rusAnswerWords.get(j).get(0));
		if (ti.scan(SecondTest.rusAnswerWords.get(j)) != 1) {
			rus_answer4 = new JLabel(SecondTest.rusAnswerWords.get(j).get(0) + ", " + SecondTest.rusAnswerWords.get(j).get(1));
		}
		rus_answer4.setBounds(232, 159, 253, 26);
		rus_answer4.setForeground(new Color(72, 61, 139));
		rus_answer4.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel.add(rus_answer4);
		j++;
		
		JLabel rus_answer5 = new JLabel(SecondTest.rusAnswerWords.get(j).get(0));
		if (ti.scan(SecondTest.rusAnswerWords.get(j)) != 1) {
			rus_answer5 = new JLabel(SecondTest.rusAnswerWords.get(j).get(0) + ", " + SecondTest.rusAnswerWords.get(j).get(1));
		}
		rus_answer5.setBounds(232, 196, 253, 26);
		panel.add(rus_answer5);
		rus_answer5.setForeground(new Color(72, 61, 139));
		rus_answer5.setFont(new Font("Tahoma", Font.PLAIN, 20));
		j++;
		
		JButton Exit = new JButton("Back to menu");
		Exit.setBackground(new Color(255, 240, 245));
		Exit.setFont(new Font("Tahoma", Font.BOLD, 14));
		Exit.setForeground(new Color(72, 61, 139));
		Exit.setBounds(318, 222, 195, 38);
		panel.add(Exit);
		try {
			Exit.setIcon(new ImageIcon(ImageIO.read(new File("Files/ic_undo_black_48dp.png"))));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		JLabel lblNewLabel_1 = new JLabel("Score " + SecondTest.score);
		lblNewLabel_1.setFont(new Font("Ravie", Font.ITALIC, 15));
		lblNewLabel_1.setForeground(new Color(75, 0, 130));
		lblNewLabel_1.setBounds(232, 222, 115, 28);
		panel.add(lblNewLabel_1);
		
		image_1 = new JLabel("");
		image_1.setVisible(false);
		image_1.setIcon(new ImageIcon(ImageIO.read(new File("Files/back.png"))));
		image_1.setBounds(24, 48, 24, 24);
		panel.add(image_1);
		
		image_2 = new JLabel("");
		image_2.setVisible(false);
		image_2.setIcon(new ImageIcon(ImageIO.read(new File("Files/back.png"))));
		image_2.setBounds(24, 87, 24, 24);
		panel.add(image_2);
		
		image_3 = new JLabel("");
		image_3.setVisible(false);
		image_3.setIcon(new ImageIcon(ImageIO.read(new File("Files/back.png"))));
		image_3.setBounds(24, 124, 24, 24);
		panel.add(image_3);
		
		image_4 = new JLabel("");
		image_4.setVisible(false);
		image_4.setIcon(new ImageIcon(ImageIO.read(new File("Files/back.png"))));
		image_4.setBounds(22, 159, 24, 24);
		panel.add(image_4);
		
		image_5 = new JLabel("");
		image_5.setVisible(false);
		image_5.setIcon(new ImageIcon(ImageIO.read(new File("Files/back.png"))));
		image_5.setBounds(22, 196, 24, 24);
		panel.add(image_5);
		
		if (SecondTest.rusAns.get(0) == 1){ 
			image_1.setVisible(true);
		}
		if (SecondTest.rusAns.get(1) == 1){
			image_2.setVisible(true);
		} 
		if (SecondTest.rusAns.get(2) == 1){
			image_3.setVisible(true);
		}
		if (SecondTest.rusAns.get(3) == 1){
			image_4.setVisible(true);
		} 
		if (SecondTest.rusAns.get(4) == 1){
			image_5.setVisible(true);
		}
		
		Exit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Main m = new Main();
				m.setUsername(username);
				m.change();
				m.upDate();
				SecondTest.action = 0;
				SecondTest.score = 0;
				SecondTest.englishAnswerWord.clear();
				SecondTest.rusAnswerWords.clear();
				SecondTest.rusAns.clear();
				SecondTest.engAns.clear();
				i=0;
				j=0;
				frmAnswer.dispose();
			}
		});
	}
	
	public void addUser(String username) {
		this.username = username;
	}
}

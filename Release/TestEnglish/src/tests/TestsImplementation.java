package tests;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import choose_of_tests.Main;

public class TestsImplementation {
	private static ArrayList<HashMap<String, ArrayList<String>>> dict = new ArrayList<HashMap<String, ArrayList<String>>>(); // ������ �������� ��� �����
	private static ArrayList<Integer> numbers = new ArrayList<Integer>();
	private ArrayList<HashMap<String, ArrayList<String>>> learning = Main.getLerning();
	private Random r = new Random();
	
	public TestsImplementation(int amount) {
	// ��������� ������ ���������������� ������� �� �������
		dict.clear();
		numbers.clear();
		int i = 0;
		while (i != amount) {
			int count = (int) r.nextInt(learning.size());
			if (numbers.contains(count)) {
				continue;
			} else {
				i++;
				numbers.add(count);
				dict.add(learning.get(count));
			}
		}
	}
	
	public ArrayList<HashMap<String, ArrayList<String>>> getDict() {
	// ���������� ������� �������
		return dict;
	}
	
	public int scan(ArrayList<String> word) {
	// �������� �� ���� ��� ��� ����� ���� � ��������
		return word.size();
	}
	
	public boolean isDigit(String s) {
	// �������� ������� �� �����������
			try {
	            Integer.parseInt(s);
	        } catch (NumberFormatException e) {
	            return true;
	        }
	        return false;
	    }
}


package tests;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import choose_of_tests.Main;
import dictionary.Dictionary;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Set;

import javax.swing.JEditorPane;
import javax.swing.JButton;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import java.awt.Color;

public class FirstTest {
	
	private Dictionary d = new Dictionary(Main.getSettings());
	private JEditorPane rightanswer1, rightanswer2, rightanswer3, rightanswer4, rightanswer5;
	private JButton btnDone, btnrepeat;
	private JFrame frmFirstTest;
	private JTextField answer1, answer2, answer3, answer4, answer5 = null;
	private JLabel lblNewLabel, proposition;
	private TestsImplementation fti = new TestsImplementation(5);
	private ArrayList<String> englishWord = new ArrayList<>(); // ������ ����
	private ArrayList<ArrayList<String>> rusWords = new ArrayList<ArrayList<String>>(); // ������ ���������
	private ArrayList<Integer> numbers = new ArrayList<Integer>(); // ������ �������, ��� � �� �����������, ������ �������
	private ArrayList<Integer> answers = new ArrayList<Integer>(); // ������ ������� ������������
	private static int score = 0; // ���������� �����
	private String username = ""; // ��� ������������
	private int times = 0; // ���������� ������� �� ������
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FirstTest window = new FirstTest();
					window.frmFirstTest.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public FirstTest() {
		initialize();		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		KeyHandler listener = new KeyHandler();

		int i = 0;
		int i1 = 1;
		collectWords();
		
		frmFirstTest = new JFrame();
		frmFirstTest.setResizable(false);
		frmFirstTest.setTitle("First Test");
		frmFirstTest.setBounds(100, 100, 886, 443);
		frmFirstTest.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmFirstTest.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(214,255,140));
		panel.setBounds(0, 0, 880, 414);
		frmFirstTest.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel english1 = new JLabel(i1 + ") " + englishWord.get(i)); // ���������� �����
		english1.setForeground(new Color(0, 0, 205));
		english1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		english1.setBounds(31, 44, 252, 44);
		panel.add(english1); i1++; i++;
		
		JLabel english2 = new JLabel(i1 + ") " + englishWord.get(i));
		english2.setForeground(new Color(0, 0, 205));
		english2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		english2.setBounds(31, 100, 252, 44);
		panel.add(english2); i1++; i++;
		
		JLabel english3 = new JLabel(i1 + ") " + englishWord.get(i));
		english3.setForeground(new Color(0, 0, 205));
		english3.setFont(new Font("Tahoma", Font.PLAIN, 20));
		english3.setBounds(31, 155, 252, 44);
		panel.add(english3); i1++; i++;
		
		JLabel english4 = new JLabel(i1 + ") " + englishWord.get(i));
		english4.setForeground(new Color(0, 0, 205));
		english4.setFont(new Font("Tahoma", Font.PLAIN, 20));
		english4.setBounds(31, 210, 252, 44);
		panel.add(english4); i1++; i++;
		
		JLabel english5 = new JLabel(i1 + ") " + englishWord.get(i));
		english5.setForeground(new Color(0, 0, 205));
		english5.setFont(new Font("Tahoma", Font.PLAIN, 20));
		english5.setBounds(31, 265, 252, 44);
		panel.add(english5);
		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// ������� �����
		i = generateNumber();
		JLabel russian1 = new JLabel(rusWords.get(i).get(0));
		if (fti.scan(rusWords.get(i)) != 1) {
			russian1 = new JLabel(rusWords.get(i).get(0) + ", " + rusWords.get(i).get(1));
		}
		russian1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		russian1.setForeground(new Color(0, 0, 128));
		russian1.setBounds(411, 44, 362, 44);
		panel.add(russian1);
		
		i = generateNumber();
		JLabel russian2 = new JLabel(rusWords.get(i).get(0));
		if (fti.scan(rusWords.get(i)) != 1) {
			russian2 = new JLabel(rusWords.get(i).get(0) + ", " + rusWords.get(i).get(1));
		}
		russian2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		russian2.setForeground(new Color(0, 0, 128));
		russian2.setBounds(411, 100, 362, 44);
		panel.add(russian2);
		
		i = generateNumber();
		JLabel russian3 = new JLabel(rusWords.get(i).get(0));
		if (fti.scan(rusWords.get(i)) != 1) {
			russian3 = new JLabel(rusWords.get(i).get(0) + ", " + rusWords.get(i).get(1));
		}
		russian3.setFont(new Font("Tahoma", Font.PLAIN, 20));
		russian3.setForeground(new Color(0, 0, 128));
		russian3.setBounds(411, 155, 362, 44);
		panel.add(russian3);
		
		i = generateNumber();
		JLabel russian4 = new JLabel(rusWords.get(i).get(0));
		if (fti.scan(rusWords.get(i)) != 1) {
			russian4 = new JLabel(rusWords.get(i).get(0) + ", " + rusWords.get(i).get(1));
		}
		russian4.setFont(new Font("Tahoma", Font.PLAIN, 20));
		russian4.setForeground(new Color(0, 0, 128));
		russian4.setBounds(411, 210, 362, 44);
		panel.add(russian4);
		
		i = generateNumber();
		JLabel russian5 = new JLabel(rusWords.get(i).get(0));
		if (fti.scan(rusWords.get(i)) != 1) {
			russian5 = new JLabel(rusWords.get(i).get(0) + ", " + rusWords.get(i).get(1));
		}
		russian5.setFont(new Font("Tahoma", Font.PLAIN, 20));
		russian5.setForeground(new Color(0, 0, 128));
		russian5.setBounds(411, 265, 362, 44);
		panel.add(russian5);
		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		answer1 = new JTextField(); // ������ ����� �������� �������
		answer1.setForeground(new Color(0, 206, 209));
		answer1.setFont(new Font("Calisto MT", Font.BOLD | Font.ITALIC, 36));
		answer1.setBounds(326, 44, 44, 44);
		panel.add(answer1);		
		answer1.addKeyListener(listener);
		
		answer2 = new JTextField();
		answer2.setForeground(new Color(0, 206, 209));
		answer2.setFont(new Font("Calisto MT", Font.BOLD | Font.ITALIC, 36));
		answer2.setBounds(326, 100, 44, 44);
		panel.add(answer2);
		answer2.addKeyListener(listener);
		
		answer3 = new JTextField();
		answer3.setForeground(new Color(0, 206, 209));
		answer3.setFont(new Font("Calisto MT", Font.BOLD | Font.ITALIC, 36));
		answer3.setBounds(326, 155, 44, 44);
		panel.add(answer3);
		answer3.addKeyListener(listener);
		
		answer4 = new JTextField();
		answer4.setForeground(new Color(0, 206, 209));
		answer4.setFont(new Font("Calisto MT", Font.BOLD | Font.ITALIC, 36));
		answer4.setBounds(326, 210, 44, 44);
		panel.add(answer4);	
		answer4.addKeyListener(listener);
		
		answer5 = new JTextField();
		answer5.setForeground(new Color(0, 206, 209));
		answer5.setFont(new Font("Calisto MT", Font.BOLD | Font.ITALIC, 36));
		answer5.setBounds(326, 265, 44, 44);
		panel.add(answer5);
		answer5.addKeyListener(listener);
		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		proposition = new JLabel(""); // ���� �� ��� ���������
		proposition.setForeground(Color.RED);
		proposition.setFont(new Font("Ravie", Font.PLAIN, 18));
		proposition.setBounds(31, 336, 454, 51);
		panel.add(proposition);
		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		rightanswer5 = new JEditorPane();
		rightanswer5.setEditable(false);
		rightanswer5.setForeground(Color.GREEN);
		rightanswer5.setFont(new Font("Calisto MT", Font.BOLD | Font.ITALIC, 36));
		rightanswer5.setBounds(808, 265, 44, 44);
		panel.add(rightanswer5);
		rightanswer5.setVisible(false);
		
		rightanswer4 = new JEditorPane();
		rightanswer4.setEditable(false);
		rightanswer4.setForeground(Color.GREEN);
		rightanswer4.setFont(new Font("Calisto MT", Font.BOLD | Font.ITALIC, 36));
		rightanswer4.setBounds(808, 210, 44, 44);
		panel.add(rightanswer4);
		rightanswer4.setVisible(false);
		
		rightanswer3 = new JEditorPane();
		rightanswer3.setEditable(false);
		rightanswer3.setForeground(Color.GREEN);
		rightanswer3.setFont(new Font("Calisto MT", Font.BOLD | Font.ITALIC, 36));
		rightanswer3.setBounds(808, 155, 44, 44);
		panel.add(rightanswer3);
		rightanswer3.setVisible(false);
		
		rightanswer2 = new JEditorPane();
		rightanswer2.setEditable(false);
		rightanswer2.setForeground(Color.GREEN);
		rightanswer2.setFont(new Font("Calisto MT", Font.BOLD | Font.ITALIC, 36));
		rightanswer2.setBounds(808, 100, 44, 44);
		panel.add(rightanswer2);
		rightanswer2.setVisible(false);
		
		rightanswer1 = new JEditorPane();
		rightanswer1.setEditable(false);
		rightanswer1.setForeground(Color.GREEN);
		rightanswer1.setFont(new Font("Calisto MT", Font.BOLD | Font.ITALIC, 36));
		rightanswer1.setBounds(808, 44, 44, 44);
		panel.add(rightanswer1);
		rightanswer1.setVisible(false);
		
		btnrepeat = new JButton("Repeat");
		btnrepeat.setBackground(new Color(255, 250, 205));
		btnrepeat.setForeground(new Color(0, 0, 205));
		btnrepeat.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnrepeat.setBounds(481, 336, 131, 51);
		panel.add(btnrepeat);
		btnrepeat.setVisible(false);
		btnrepeat.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Main m = new Main();
				m.upDate();
				frmFirstTest.dispose();
				answers.clear();
				numbers.clear();
				englishWord.clear();
				rusWords.clear();
				fti = new TestsImplementation(5);
				initialize();
				frmFirstTest.setVisible(true);
				times = 0;
			}
		});
		
		lblNewLabel = new JLabel("Answers");
		lblNewLabel.setForeground(new Color(72, 61, 139));
		lblNewLabel.setFont(new Font("Ravie", Font.BOLD, 18));
		lblNewLabel.setBounds(354, 0, 131, 36);
		lblNewLabel.setVisible(false);
		panel.add(lblNewLabel);
		
		btnDone = new JButton("Done"); // ���������� ���������� �����
		btnDone.setForeground(new Color(0, 0, 255));
		btnDone.setBackground(new Color(255, 250, 205));
		btnDone.setFont(new Font("Tahoma", Font.PLAIN, 20));
		try {
			btnDone.setIcon(new ImageIcon(ImageIO.read(new File("Files/ic_done_black_24dp.png"))));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		btnDone.setBounds(627, 336, 225, 51);
		panel.add(btnDone);

		btnDone.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				 checkAnswers();
			}
		});
	}
	
	public void collectWords() {
	// ���������� ������� ���� � ������� ������� �� ���������
		ArrayList<HashMap<String, ArrayList<String>>> dict = fti.getDict();
		
		for (int i = 0; i < dict.size(); i++) {
			HashMap<String, ArrayList<String>> onePairOfWords = dict.get(i);
			Set<String> keys = onePairOfWords.keySet();
			for (String key: keys) { 
				englishWord.add(key);
				rusWords.add(onePairOfWords.get(key));
			}
		}
	}
	
	public int generateNumber() {
	// ��������� ������� ������� ���� � label
		Random r = new Random();
		int count = 0;
		do {
			count = (int) r.nextInt(englishWord.size());
		} while (numbers.contains(count));
		numbers.add(count);	
		System.out.println(count);
		return count;
	}	
	
	public void check() {
	// �������� ������ �� ������������
		score = 0;
		System.out.println();
		for (int i = 0; i < numbers.size(); i++) {
			int answer = answers.get(i);
			if (answer < 10) {
				answer--;
				answers.set(i, answer);
			} else {
				char answerUser = (Integer.toString(answer).toCharArray()[0]);
				answer = Integer.parseInt(String.valueOf(answerUser))-1;
				answers.set(i, answer);
			}
			System.out.println(numbers.get(i) + " -> " + answers.get(i));
			if (numbers.get(i) == answers.get(i)) {
				score++;
			}
		}
	}
	
	@SuppressWarnings("static-access")
	public void checkUsersDict(int i) {
	// ������ ���� � ��������
		ArrayList<HashMap<String, ArrayList<String>>> a = Main.getLerning();
		a.remove(fti.getDict().get(i));
		a.add(d.getWordFromMainDictionary(new Random().nextInt(d.getLength())));

		ArrayList<HashMap<String, ArrayList<String>>> b = Main.getLerned();
		b.add(fti.getDict().get(i));
		d.checkOnLearnedLearning();
	}
	
	public void change(String username) {
		frmFirstTest.setVisible(true);
		this.username = username;
	}
	
	public void checkAnswers() {
		if (times > 0) {
			Main m = new Main();
			m.setUsername(username);
			m.change();
			m.upDate();
			frmFirstTest.dispose();
			times = 0;
			score = 0;
		} else {
		if (answer1.getText().isEmpty() || answer2.getText().isEmpty() || answer3.getText().isEmpty() || answer4.getText().isEmpty() || answer5.getText().isEmpty()) {
			proposition.setForeground(Color.RED);
			proposition.setText("Answer on all");
		} else if (fti.isDigit(answer1.getText()) || fti.isDigit(answer2.getText()) || fti.isDigit(answer3.getText()) || fti.isDigit(answer4.getText()) || fti.isDigit(answer5.getText())) {
			proposition.setForeground(Color.RED);
			proposition.setText("Character is in one of the fields");
		} else {
			times++;
			btnDone.setText("Back to menu");
			try {
				btnDone.setIcon(new ImageIcon(ImageIO.read(new File("Files/ic_undo_black_48dp.png"))));
			} catch (IOException e) {
				e.printStackTrace();
			}
			frmFirstTest.setTitle("Results of the first test");
			
			answers.add(Integer.parseInt(answer1.getText()));
			answers.add(Integer.parseInt(answer2.getText()));
			answers.add(Integer.parseInt(answer3.getText()));
			answers.add(Integer.parseInt(answer4.getText()));
			answers.add(Integer.parseInt(answer5.getText()));
			check();
			
			answer1.setEditable(false);
			answer2.setEditable(false);
			answer3.setEditable(false);
			answer4.setEditable(false);
			answer5.setEditable(false);
			btnrepeat.setVisible(true);
			lblNewLabel.setVisible(true);
			
			proposition.setForeground(new Color(72, 61, 139));
			proposition.setFont(new Font("Ravie", Font.BOLD, 18));
			proposition.setText("Score " + score);
			
			if (numbers.get(0) != answers.get(0)) {
				rightanswer1.setVisible(true);
				rightanswer1.setText(Integer.toString((numbers.get(0) + 1)));
				answer1.setForeground(Color.RED);
			} else {
				answer1.setForeground(Color.GREEN);
				checkUsersDict(0);
			}
			
			if (numbers.get(1) != answers.get(1)) {
				rightanswer2.setVisible(true);
				rightanswer2.setText(Integer.toString((numbers.get(1) + 1)));
				answer2.setForeground(Color.RED);
			} else {
				answer2.setForeground(Color.GREEN);
				checkUsersDict(1);
			}
			
			if (numbers.get(2) != answers.get(2)) {
				rightanswer3.setVisible(true);
				rightanswer3.setText(Integer.toString((numbers.get(2) + 1)));
				answer3.setForeground(Color.RED);
			} else {
				answer3.setForeground(Color.GREEN);
				checkUsersDict(2);
			}
			
			if (numbers.get(3) != answers.get(3)) {
				rightanswer4.setVisible(true);
				rightanswer4.setText(Integer.toString((numbers.get(3) + 1)));
				answer4.setForeground(Color.RED);
			} else {
				answer4.setForeground(Color.GREEN);
				checkUsersDict(3);
			}
			
			if (numbers.get(4) != answers.get(4)) {
				rightanswer5.setVisible(true);
				rightanswer5.setText(Integer.toString((numbers.get(4) + 1)));
				answer5.setForeground(Color.RED);
			} else {
				answer5.setForeground(Color.GREEN);
				checkUsersDict(4);
			}
			answers.clear();
			numbers.clear();
			englishWord.clear();
			rusWords.clear();
		}
		}
	}

	private class KeyHandler implements KeyListener {

		@Override
		public void keyPressed(KeyEvent event) {
			int keyCode = event.getKeyCode();
			switch (keyCode) {	
				case KeyEvent.VK_ENTER:
					checkAnswers();
					break;
			}
		}

		@Override
		public void keyReleased(KeyEvent event) {
			// TODO Auto-generated method stub
		}

		@Override
		public void keyTyped(KeyEvent event) {
			// TODO Auto-generated method stub
		}
	}
}